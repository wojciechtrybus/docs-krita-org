msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_pdf.pot\n"

#: ../../general_concepts/file_formats/file_pdf.rst:1
msgid "The PDF file format in Krita."
msgstr "Krita 中的 PDF 文件格式。"

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "*.pdf"
msgstr ""

#: ../../general_concepts/file_formats/file_pdf.rst:10
msgid "PDF"
msgstr ""

#: ../../general_concepts/file_formats/file_pdf.rst:15
msgid "\\*.pdf"
msgstr "\\*.pdf"

#: ../../general_concepts/file_formats/file_pdf.rst:17
msgid ""
"``.pdf`` is a format intended for making sure a document looks the same on "
"all computers. It became popular because it allows the creator to make sure "
"that the document looks the same and cannot be changed by viewers. These "
"days it is an open standard and there is quite a variety of programs that "
"can read and save PDFs."
msgstr ""

#: ../../general_concepts/file_formats/file_pdf.rst:19
msgid ""
"Krita can open PDFs with multiple layers. There is currently no PDF export, "
"nor is that planned. If you want to create a PDF with images from Krita, use "
"`Scribus <https://www.scribus.net/>`_."
msgstr ""

#: ../../general_concepts/file_formats/file_pdf.rst:21
msgid ""
"While PDFs can be viewed via most browsers, they can also become very heavy "
"and are thus not recommended outside of official documents. Printhouses will "
"often accept PDF."
msgstr ""
