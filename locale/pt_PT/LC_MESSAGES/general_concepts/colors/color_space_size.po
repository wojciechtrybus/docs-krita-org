# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-19 23:35+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: colorcategory Krita image spacesv\n"
"X-POFile-SpellExtra: Basiccolormanagementcompare spaces jpg images\n"
"X-POFile-SpellExtra: spacesnonmanaged Basiccolormanagementgradientsin en\n"
"X-POFile-SpellExtra: LUT CMYK Mediawiki sRGB ACES sRGBtrc\n"

#: ../../general_concepts/colors/color_space_size.rst:None
msgid ""
".. image:: images/color_category/Basiccolormanagement_compare4spaces.png"
msgstr ""
".. image:: images/color_category/Basiccolormanagement_compare4spaces.png"

#: ../../general_concepts/colors/color_space_size.rst:1
msgid "About Color Space Size"
msgstr "Acerca do Tamanho do Espaço de Cores"

#: ../../general_concepts/colors/color_space_size.rst:10
msgid "Color"
msgstr "Cor"

#: ../../general_concepts/colors/color_space_size.rst:10
msgid "Color Spaces"
msgstr "Espaços de Cores"

#: ../../general_concepts/colors/color_space_size.rst:15
msgid "Color Space Size"
msgstr "Tamanho do Espaço de Cores"

#: ../../general_concepts/colors/color_space_size.rst:17
msgid ""
"Using Krita's color space browser, you can see that there are many different "
"space sizes."
msgstr ""
"Usando o selector de espaços de cores do Krita, poderá ver que existem "
"diversos tamanhos de espaços."

#: ../../general_concepts/colors/color_space_size.rst:25
msgid "How do these affect your image, and why would you use them?"
msgstr "Como é que estes afectam a sua imagem e como é que os poderá usar?"

#: ../../general_concepts/colors/color_space_size.rst:27
msgid "There are three primary reasons to use a large space:"
msgstr "Existem três razões principais para usar um espaço grande:"

#: ../../general_concepts/colors/color_space_size.rst:29
msgid ""
"Even though you can't see the colors, the computer program does understand "
"them and can do color maths with it."
msgstr ""
"Ainda que não consiga ver as cores, o programa do computador consegue "
"compreendê-las e fazer as contas matemáticas com elas."

#: ../../general_concepts/colors/color_space_size.rst:30
msgid ""
"For exchanging between programs and devices: most CMYK profiles are a little "
"bigger than our default sRGB in places, while in other places, they are "
"smaller. To get the best conversion, having your image in a space that "
"encompasses both your screen profile as your printer profile."
msgstr ""
"Para fazer o intercâmbio entre os programas e os dispositivos: a maioria dos "
"perfis CMYK são um pouco maiores que o nosso perfil sRGB predefinido nalguns "
"locais, enquanto noutros locais é mais pequeno. Para obter a melhor "
"conversão, ter a sua imagem num espaço que consiga conter o perfil do seu "
"ecrã, assim como o perfil da sua impressora."

#: ../../general_concepts/colors/color_space_size.rst:31
msgid ""
"For archival purposes. In other words, maybe monitors of the future will "
"have larger amounts of colors they can show (spoiler: they already do), and "
"this allows you to be prepared for that."
msgstr ""
"Por questões de arquivo. Por outras palavras, talvez os monitores do futuro "
"terão maiores quantidades de cores que conseguem mostrar (pormenor "
"indiscreto: já o fazem) e isto permite-lhe estar preparado para isso."

#: ../../general_concepts/colors/color_space_size.rst:33
msgid "Let's compare the following gradients in different spaces:"
msgstr "Vamos comparar os seguintes gradientes em diferentes espaços:"

#: ../../general_concepts/colors/color_space_size.rst:37
msgid ""
".. image:: images/color_category/Basiccolormanagement_gradientsin4spaces_v2."
"jpg"
msgstr ""
".. image:: images/color_category/Basiccolormanagement_gradientsin4spaces_v2."
"jpg"

#: ../../general_concepts/colors/color_space_size.rst:40
msgid ""
".. image:: images/color_category/"
"Basiccolormanagement_gradientsin4spaces_nonmanaged.png"
msgstr ""
".. image:: images/color_category/"
"Basiccolormanagement_gradientsin4spaces_nonmanaged.png"

#: ../../general_concepts/colors/color_space_size.rst:41
msgid ""
"On the left we have an artifact-ridden color managed jpeg file with an ACES "
"sRGBtrc v2 profile attached (or not, if not, then you can see the exact "
"different between the colors more clearly). This should give an "
"approximation of the actual colors. On the right, we have an sRGB png that "
"was converted in Krita from the base file."
msgstr ""
"Do lado esquerdo, temos um ficheiro JPEG com gestão de cores e livre de "
"artefactos, com um perfil ACES sRGBtrc v2 associado (ou não, dependendo da "
"opinião do Mediawiki; se não for o caso, poderá ver a diferença exacta entre "
"as cores de forma mais clara). Isto deve-lhe dar uma aproximação das cores "
"actuais. Do lado direito, temos um PNG em sRGB que foi convertido no Krita a "
"partir do ficheiro de base."

#: ../../general_concepts/colors/color_space_size.rst:43
msgid ""
"Each of the gradients is the gradient from the max of a given channel. As "
"you can see, the mid-tone of the ACES color space is much brighter than the "
"mid-tone of the RGB colorspace, and this is because the primaries are "
"further apart."
msgstr ""
"Cada um dos gradientes é o gradiente do máximo de um dado canal. Como pode "
"ver, o tom intermédio do espaço de cores ACES é muito mais claro que o tom "
"intermédio do espaço de cores RGB, e isto acontece porque as cores primárias "
"estão mais afastadas."

#: ../../general_concepts/colors/color_space_size.rst:45
msgid ""
"What this means for us is that when we start mixing or applying filters, "
"Krita can output values higher than visible, but also generate more correct "
"mixes and gradients. In particular, when color correcting, the bigger space "
"can help with giving more precise information."
msgstr ""
"O que isto significa para nós é que, quando começarmos a misturar ou a "
"aplicar filtros, o Krita consegue gerar valores mais elevados que os "
"visíveis, mas também gera misturas e gradientes mais correctos. Em "
"particular, quando estiver a fazer a correcção de cores, um espaço maior "
"poderá a atribuir uma informação mais precisa."

#: ../../general_concepts/colors/color_space_size.rst:47
msgid ""
"If you have a display profile that uses a LUT, then you can use perceptual "
"to give an indication of how your image will look."
msgstr ""
"Se tiver um perfil de visualização que use um LUT, então poderá usar a "
"percepção para dar uma indicação da aparência com que irá ficar a sua imagem."

#: ../../general_concepts/colors/color_space_size.rst:49
msgid ""
"Bigger spaces do have the downside they require more precision if you do not "
"want to see banding, so make sure to have at the least 16bit per channel "
"when choosing a bigger space."
msgstr ""
"Os espaços maiores têm o problema que exigem mais precisão, caso não queira "
"ver a existência de bandas, por isso certifique-se que tem pelo menos 16 "
"bits por canal quando escolher um espaço maior."
