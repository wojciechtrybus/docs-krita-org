# Translation of docs_krita_org_reference_manual___main_menu___settings_menu.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___settings_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:40+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ".. image:: images/preferences/Configure_Toolbars_Krita.png"
msgstr ".. image:: images/preferences/Configure_Toolbars_Krita.png"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ""
".. image:: images/preferences/Configure_Toolbars_Brushes_and_Stuff_Custom.png"
msgstr ""
".. image:: images/preferences/Configure_Toolbars_Brushes_and_Stuff_Custom.png"

#: ../../reference_manual/main_menu/settings_menu.rst:None
msgid ".. image:: images/preferences/Toolbars_Shown.png"
msgstr ".. image:: images/preferences/Toolbars_Shown.png"

#: ../../reference_manual/main_menu/settings_menu.rst:1
msgid "The settings menu in Krita."
msgstr "Меню параметрів у Krita."

#: ../../reference_manual/main_menu/settings_menu.rst:17
msgid "Setting Menu"
msgstr "Меню «Параметри»"

#: ../../reference_manual/main_menu/settings_menu.rst:19
msgid ""
"The Settings Menu houses the configurable options in Krita and where you "
"determine most of the \"look and feel\" of the application."
msgstr ""
"За допомогою меню «Параметри» ви можете отримати доступ до параметрів "
"налаштовування Krita та визначення більшої частини того, як виглядає і "
"поводиться програма."

#: ../../reference_manual/main_menu/settings_menu.rst:21
#: ../../reference_manual/main_menu/settings_menu.rst:34
msgid "Dockers"
msgstr "Бічні панелі"

#: ../../reference_manual/main_menu/settings_menu.rst:24
#: ../../reference_manual/main_menu/settings_menu.rst:31
msgid "Show Dockers"
msgstr "Показати панелі"

#: ../../reference_manual/main_menu/settings_menu.rst:27
msgid ""
"Determines whether or not the dockers are visible.  This is a nice aid to "
"cleaning up the interface and removing unnecessary \"eye-ball clutter\" when "
"you are painting.  If you've got your brush and you know you're just going "
"to be painting for awhile why not flip the dockers off?  You'd be amazed "
"what a difference it makes while you're working.  However, if you know "
"you're swapping out tools or working with layer or any of the other myriad "
"things Krita lets you do then there's no point getting caught up in flipping "
"the docks on and off.  Use you time wisely!"
msgstr ""
"Визначає, чи будуть бічні панелі видимими. Тимчасове прибирання панелей може "
"бути корисним для спрощення інтерфейсу та прибирання зайвих візуальних "
"елементів під час малювання. Якщо у вас є потрібний пензель і ви просто "
"хочете зосередитися на малюванні тим, чому б на певний час не вимкнути показ "
"бічних панелей? Певні, без них вам буде набагато зручніше працювати. Втім, "
"якщо ви знаєте, що часто мінятимете інструменти, працюватимете із шарами або "
"багатьма тими речами, доступ до яких у Krita здійснюється за допомогою "
"бічних панелей, немає сенсу у постійному вмиканні або вимиканні показу "
"бічних панелей. Ставтеся ощадливо до свого часу!"

#: ../../reference_manual/main_menu/settings_menu.rst:31
msgid ""
"This is a great candidate to add to the toolbar so you can just click the "
"dockers on and off and don't even have to open the menu to do it. See :ref:"
"`configure_toolbars` below for more."
msgstr ""
"Відповідна кнопка є непоганим кандидатом для додавання на панель "
"інструментів. Її буде зручно натискати для вмикання і вимикання бічних "
"панелей — для цього не треба буде відкривати ніяке меню. Щоб дізнатися про "
"це більше, ознайомтеся із розділом :ref:`configure_toolbars`."

#: ../../reference_manual/main_menu/settings_menu.rst:36
msgid ""
"Krita subdivides the access of many of its features into functional panels "
"called Dockers. Dockers are small windows that can contain, for example, "
"things like the Layer Stack, Color Palette or Brush Presets. Think of them "
"as the painter's palette, or his water, or his brushkit."
msgstr ""
"У Krita доступ до багатьох можливостей здійснюється за допомогою окремих "
"функціональних панелей, які ми називаємо бічними панелями. Бічні панелі — "
"невеликі віконця, які можуть містити, наприклад, стос шарів, палітру "
"кольорів або набори пензлів. Ці панелі можна уявляти собі як палітру, набір "
"акварелей або футляр для пензлів традиційного художника."

#: ../../reference_manual/main_menu/settings_menu.rst:38
msgid ""
"Learning to use dockers effectively is a key concept to optimizing your time "
"using Krita."
msgstr ""
"Добрі знання щодо бічних панелей є ключем до оптимізації ваших робочих "
"процесів у Krita."

#: ../../reference_manual/main_menu/settings_menu.rst:40
#: ../../reference_manual/main_menu/settings_menu.rst:43
msgid "Themes"
msgstr "Теми"

#: ../../reference_manual/main_menu/settings_menu.rst:40
msgid "Theme"
msgstr "Тема"

#: ../../reference_manual/main_menu/settings_menu.rst:40
msgid "Look and Feel"
msgstr "Вигляд і поведінка"

#: ../../reference_manual/main_menu/settings_menu.rst:45
msgid ""
"Krita provides a number of color-themed interfaces or \"looks\".  The "
"current set of themes are the following:"
msgstr ""
"У Krita передбачено декілька інтерфейсів різних кольорів або «тем». У "
"поточній версії набір тем є таким:"

#: ../../reference_manual/main_menu/settings_menu.rst:47
msgid "Dark (Default)"
msgstr "Темний (типовий)"

#: ../../reference_manual/main_menu/settings_menu.rst:48
msgid "Blender"
msgstr "Blender"

#: ../../reference_manual/main_menu/settings_menu.rst:49
msgid "Bright"
msgstr "Яскрава"

#: ../../reference_manual/main_menu/settings_menu.rst:50
msgid "Neutral"
msgstr "Нейтральна"

#: ../../reference_manual/main_menu/settings_menu.rst:52
msgid ""
"There is no easy way to create and share themes. The color themes are "
"defined in the :menuselection:`Share --> Color Schemes` folder where Krita "
"is downloaded."
msgstr "Поки не існує простого способу створення і оприлюднення тем."

#: ../../reference_manual/main_menu/settings_menu.rst:55
msgid "Configure Shortcuts"
msgstr "Налаштувати скорочення"

#: ../../reference_manual/main_menu/settings_menu.rst:57
msgid ""
"Configuring shortcuts is another way to customize the application to fit "
"you.  Whether you are transitioning from another app, like Photoshop or "
"MyPaint, or you think your own shortcut keys make more sense for you then "
"Krita has got you covered.  You get to the shortcuts interface through :"
"menuselection:`Settings --> Configure Krita`  and by choosing the :"
"menuselection:`Keyboard Shortcuts`  tab."
msgstr ""
"Налаштовування клавіатурних скорочень є одним зі способів зробити "
"користування програмою зручнішим саме для вас. Переходите ви з іншої "
"програми, зокрема Photoshop або MyPaint, чи просто вважаєте, що якась "
"комбінація клавіш для вас буде зручнішою за типову, Krita можна налаштувати "
"у найзручніший спосіб. Інтерфейс налаштовування клавіатурних скорочень можна "
"викликати за допомогою пункту меню :menuselection:`Параметри --> "
"Налаштувати` і наступним відкриттям вкладки :menuselection:`Клавіатурні "
"скорочення`."

#: ../../reference_manual/main_menu/settings_menu.rst:59
msgid ""
"To use, just type the :guilabel:`Action` into the Search box you want to "
"assign/reassign the shortcut for.  Suppose we wanted to assign the shortcut :"
"kbd:`Ctrl + G` to the :guilabel:`Action` of Group Layers so that every time "
"we pressed the :kbd:`Ctrl + G` shortcut a new Layer Group would be created.  "
"Use the following steps to do this:"
msgstr ""
"Щоб скористатися панеллю налаштовування, просто введіть назву дії, для якої "
"ви хочете призначити або змінити клавіатурне скорочення, до поля пошуку. "
"Припустімо, що ми хочемо призначити клавіатурне скорочення :kbd:`Ctrl + G` "
"до дії :guilabel:`Згрупувати шари`, щоб кожного разу, коли ви натискатимете :"
"kbd:`Ctrl + G`, буде створено групу шарів. Для досягнення мети виконайте "
"такі дії:"

#: ../../reference_manual/main_menu/settings_menu.rst:61
msgid "Type \"Group Layer\"."
msgstr "Введіть «Група шарів»."

#: ../../reference_manual/main_menu/settings_menu.rst:62
msgid "Click on Group Layer and a small inset box will open."
msgstr "Натисніть на групі шарів, і програма покаже невеличку вставну панель."

#: ../../reference_manual/main_menu/settings_menu.rst:63
msgid "Click the Custom radio button."
msgstr "Позначте пункт «Нетиповий»."

#: ../../reference_manual/main_menu/settings_menu.rst:64
msgid "Click on the first button and type the :kbd:`Ctrl + G` shortcut."
msgstr ""
"Натисніть першу кнопку і скористайтеся натисканням комбінації клавіш :kbd:"
"`Ctrl + G`."

#: ../../reference_manual/main_menu/settings_menu.rst:65
msgid "Click OK."
msgstr "Натисніть кнопку «Гаразд»."

#: ../../reference_manual/main_menu/settings_menu.rst:67
msgid ""
"From this point on, whenever you press the :kbd:`Ctrl + G` shortcut you'll "
"get a new :guilabel:`Group Layer`."
msgstr ""
"З цього моменту, коли ви натиснете комбінацію клавіш :kbd:`Ctrl + G`, "
"програма створюватиме :guilabel:`Групу шарів`."

#: ../../reference_manual/main_menu/settings_menu.rst:70
msgid ""
"Smart use of shortcuts can save you significant time and further streamline "
"your workflow."
msgstr ""
"Розумне використання клавіатурних скорочень може значно заощадити час і "
"оптимізувати ваші робочі процедури."

#: ../../reference_manual/main_menu/settings_menu.rst:73
msgid "Manage Resources"
msgstr "Керування ресурсами"

#: ../../reference_manual/main_menu/settings_menu.rst:75
msgid ""
"Manage the resources. You can read more about it :ref:`here "
"<resource_management>`."
msgstr ""
"Керування ресурсами. Докладніше про це :ref:`тут <resource_management>`."

#: ../../reference_manual/main_menu/settings_menu.rst:77
msgid "Language"
msgstr "Мова"

#: ../../reference_manual/main_menu/settings_menu.rst:81
msgid "Switch Application Language"
msgstr "Перемкнути мову програми"

#: ../../reference_manual/main_menu/settings_menu.rst:83
msgid "If you wish to use Krita in a different translation."
msgstr "Якщо ви хочете змінити переклад інтерфейсу Krita на інший."

#: ../../reference_manual/main_menu/settings_menu.rst:85
msgid "Toolbar"
msgstr "Панель інструментів"

#: ../../reference_manual/main_menu/settings_menu.rst:89
msgid "Configure Toolbars"
msgstr "Налаштувати пенали"

#: ../../reference_manual/main_menu/settings_menu.rst:91
msgid ""
"Krita allows you to highly customize the Toolbar interface.  You can add, "
"remove and change the order of nearly everything to fit your style of work.  "
"To get started, choose :menuselection:`Settings --> Configure Toolbars`."
msgstr ""
"У Krita передбачено дуже широкі можливості з налаштовування інтерфейсу "
"панелі інструментів. Ви можете додавати, вилучати або змінювати порядок "
"майже будь-яких елементів так, щоб вони відповідали стилю вашої роботи. Для "
"початку, скористайтеся пунктом меню :menuselection:`Параметри --> "
"Налаштувати пенали`."

#: ../../reference_manual/main_menu/settings_menu.rst:96
msgid "The dialog is broken down into three main sections:"
msgstr "Це вікно розділено на три основні частини:"

#: ../../reference_manual/main_menu/settings_menu.rst:98
msgid "The Toolbar"
msgstr "Панель інструментів"

#: ../../reference_manual/main_menu/settings_menu.rst:99
msgid "Choose to either modify the \"File\" or \"Brushes and Stuff\" toolbars."
msgstr "Виберіть панель для внесення змін — «Файл» або «Пензлі та інше»."

#: ../../reference_manual/main_menu/settings_menu.rst:100
msgid "Available Actions:"
msgstr "Доступні кнопки дій:"

#: ../../reference_manual/main_menu/settings_menu.rst:101
msgid "All the options that can be added to a toolbar."
msgstr "Усі пункти, які можна додати на панель інструментів."

#: ../../reference_manual/main_menu/settings_menu.rst:103
msgid "Current Actions:"
msgstr "Поточні дії:"

#: ../../reference_manual/main_menu/settings_menu.rst:103
msgid "All the actions currently assigned and the order they are in."
msgstr "Усі пункти дій, які можна пов'язати із панелями та їхній порядок."

#: ../../reference_manual/main_menu/settings_menu.rst:105
msgid ""
"Use the arrows between the *Available* and *Current* actions sections to "
"move items back and forth and up and down in the hierarchy.  This type of "
"inclusion/exclusion interface has been around on PCs for decades so we don't "
"need to go into great detail regarding its use.  What is important though is "
"selecting the correct Toolbar to work on.  The :guilabel:`File` Toolbar "
"allows you to add items between the :menuselection:`New` , :menuselection:"
"`Open`  and :menuselection:`Save`  buttons as well as to the right of the :"
"menuselection:`Save`  button.  The :guilabel:`Brushes and Stuff` Toolbar, "
"lets you modify anything from the Gradients button over to the right.  This "
"is probably where you'll do most of your editing."
msgstr ""
"Скористайтеся стрілками між списками *Наявні дії* і *Поточні дії* для "
"пересування пунктів дій з одного списку до іншого, а також вгору або вниз "
"ієрархією. Цей тип інтерфейсу додавання-вилучення працює на комп'ютерах уже "
"десятиліття, отже, мабуть, не варто надто багато пояснювати. Важливим є лише "
"вибір належної панелі інструментів. На панель інструментів :guilabel:`Файл` "
"можна додавати пункти між кнопками :menuselection:`Створити` , :"
"menuselection:`Відкрити` і :menuselection:`Зберегти`, також праворуч від "
"кнопки :menuselection:`Зберегти`. На панель інструментів :guilabel:`Пензлі "
"та інше` можна додавати кнопки, починаючи з кнопки «Градієнти», і далі "
"праворуч. Ймовірно, саме цю панель ви редагуватимете найчастіше."

#: ../../reference_manual/main_menu/settings_menu.rst:107
msgid ""
"Here we've added :menuselection:`Select Opaque` , :menuselection:`Local "
"Selection` ,  :menuselection:`Transparency Mask` , :guilabel:`Isolate "
"Layer` , :menuselection:`Show Assistant Previews` .  This is just an example "
"of a couple of options that are used frequently and might trim your "
"workflow.   This is what it looks like in the configuration tool:"
msgstr ""
"У нашому прикладі ми додали дії :menuselection:`Вибрати непрозоре`, :"
"menuselection:`Локальне позначення`,  :menuselection:`Маска прозорості`, :"
"guilabel:`Ізолювати шар`, :menuselection:`Показати попередні перегляди "
"допоміжних засобів`. Це лише приклад декількох пунктів дій, які часто "
"використовують, і які можуть спростити ваш робочий процес. Ось, як це "
"виглядає у вікні засобу налаштовування:"

#: ../../reference_manual/main_menu/settings_menu.rst:112
msgid ""
"You'll notice that some of the items are text only and some only icons.  "
"This is determined by whether the particular item has an associated icon in "
"Krita.  You can select anything from the *Available* section and move it to "
"the *Current* one and rearrange to fit your own workflow."
msgstr ""
"Ви можете помітити, що для деяких пунктів показано лише текст, а для деяких "
"— текст із піктограмами. Вигляд пункту визначається тим, чи пов'язано із ним "
"певну піктограму у Krita. Ви можете вибрати будь-який пункт у списку *Наявні "
"дії* і пересунути його до списку *Поточні дії*, а також змінити порядок "
"пунктів за власним уподобанням."

#: ../../reference_manual/main_menu/settings_menu.rst:114
msgid ""
"If you add so many that they won't all fit on your screen at once, you will "
"see a small chevron icon appear.  Click it and the toolbar expands to show "
"the remaining items."
msgstr ""
"Якщо пунктів буде так багато, що вони не вміщатимуться на панелі, програма "
"покаже невеличку піктограму-шеврон у правому кінці панелі. Якщо ви натиснете "
"цю піктограму, буде розкрито додаткову панель із прихованими кнопками."

#: ../../reference_manual/main_menu/settings_menu.rst:117
msgid "Toolbars shown"
msgstr "Показані панелі"

#: ../../reference_manual/main_menu/settings_menu.rst:117
msgid "Gives a list of toolbars that can be shown."
msgstr "Містить список панелей, які може бути показано."

#: ../../reference_manual/main_menu/settings_menu.rst:119
msgid ""
"At this time Krita does not support the ability to create additional "
"toolbars. The ones available are:"
msgstr ""
"У поточній версії Krita не передбачено підтримки створення додаткових "
"панелей інструментів. Доступними є такі панелі інструментів:"

#: ../../reference_manual/main_menu/settings_menu.rst:124
msgid ""
"Although not really advisable, you can turn them off (but why would you.."
"really?)"
msgstr ""
"Хоча ми не радимо цього робити, ви можете вимкнути показ усіх цих панелей "
"(але навіщо?)."

#: ../../reference_manual/main_menu/settings_menu.rst:126
msgid ""
"Finally, Toolbars also can be moved. You can do this by |mouseright| and "
"dragging the handler at the left side of the toolbar."
msgstr ""
"Нарешті, панелі інструментів можна пересувати. Зробити це можна натиснувши |"
"mouseright| і перетягнувши панель за елемент керування на лівому боці панелі "
"інструментів."
