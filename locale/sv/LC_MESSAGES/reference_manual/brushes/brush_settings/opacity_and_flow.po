# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:1
msgid "Opacity and flow in Krita."
msgstr "Ogenomskinlighet och flöde i Krita."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:27
msgid "Opacity"
msgstr "Ogenomskinlighet"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid "Flow"
msgstr "Flöde"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
msgid "Transparency"
msgstr "Genomskinlighet"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:17
msgid "Opacity and Flow"
msgstr "Ogenomskinlighet och flöde"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:19
msgid "Opacity and flow are parameters for the transparency of a brush."
msgstr ""
"Ogenomskinlighet och flöde är parametrar för en pensels genomskinlighet."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:22
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:23
msgid "They are interlinked with the painting mode setting."
msgstr "De är länkade till inställningarna av målningsmetod."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:26
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:28
msgid "The transparency of a stroke."
msgstr "Ett drags genomskinlighet."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid ""
"The transparency of separate dabs. Finally separated from Opacity in 2.9."
msgstr ""
"Genomskinlighet för separata klickar. Slutligen skild från ogenomskinlighet "
"i 2.9."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:34
msgid ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"
msgstr ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:35
msgid ""
"In Krita 4.1 and below, the flow and opacity when combined with brush "
"sensors would add up to one another, being only limited by the maximum "
"opacity. This was unexpected compared to all other painting applications, so "
"in 4.2 this finally got corrected to the flow and opacity multiplying, "
"resulting in much more subtle strokes. This change can be switched back in "
"the :ref:`tool_options_settings`, but we will be deprecating the old way in "
"future versions."
msgstr ""
"I Krita 4.1 och tidigare, adderades flödet och ogenomskinligheten till "
"varandra när de kombinerades med penselsensorer, bara begränsade av den "
"maximala ogenomskinligheten. Det var oväntat jämfört med alla andra "
"målarprogram, så i 4.2 rättades det till sist så att flödet och "
"ogenomskinligheten multipliceras, vilket resulterar i mycket subtilare "
"penseldrag. Ändringen kan bytas tillbaka under :ref:`tool_options_settings`, "
"men vi kommer att avråda från användning av det gamla sättet i framtida "
"versioner."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:38
msgid "The old behavior can be simulated in the new system by..."
msgstr "Det gamla beteendet kan simuleras i det nya systemet genom att ..."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:40
msgid "Deactivating the sensors on opacity."
msgstr "Inaktivera sensorn för ogenomskinlighet."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:41
msgid "Set the maximum value on flow to 0.5."
msgstr "Ställ in flödets maximala värde till 0,5."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:42
msgid "Adjusting the pressure curve to be concave."
msgstr "Justera tryckkurvan så att den är konkav."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:44
msgid ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"
msgstr ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:49
msgid "Painting mode"
msgstr "Målningsmetod"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:51
msgid "Build-up"
msgstr "Uppbyggnad"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:52
msgid "Will treat opacity as if it were the same as flow."
msgstr "Behandlar ogenomskinlighet som om det var samma sak som flöde."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Wash"
msgstr "Sprid ut"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Will treat opacity as stroke transparency instead of dab-transparency."
msgstr ""
"Behandlar ogenomskinlighet som draggenomskinlighet istället för "
"klickgenomskinlighet."

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:58
msgid ""
"Where the other images of this page had all three strokes set to painting "
"mode: wash, this one is set to build-up."
msgstr ""
"Där övriga bilder på sidan alla har tre drag inställda till målningsmetoden "
"Sprid ut, är den här inställd till Uppbyggnad."
