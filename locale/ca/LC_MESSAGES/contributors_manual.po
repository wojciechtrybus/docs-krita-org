# Translation of docs_krita_org_contributors_manual.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: contributors_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-04 19:30+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../contributors_manual.rst:5
msgid "Contributors Manual"
msgstr "Manual dels col·laboradors"

#: ../../contributors_manual.rst:7
msgid "Everything you need to know to help out with Krita!"
msgstr "Tot el que cal conèixer per ajudar amb el Krita!"

#: ../../contributors_manual.rst:9
msgid "Contents:"
msgstr "Contingut:"
