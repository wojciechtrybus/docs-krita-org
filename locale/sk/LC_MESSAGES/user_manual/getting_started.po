# translation of docs_krita_org_user_manual___getting_started.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___getting_started\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-22 08:58+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../user_manual/getting_started.rst:5
msgid "Getting Started"
msgstr "Začíname"

#: ../../user_manual/getting_started.rst:7
msgid ""
"Welcome to the Krita Manual! In this section, we'll try to get you up to "
"speed."
msgstr ""

#: ../../user_manual/getting_started.rst:9
msgid ""
"If you are familiar with digital painting, we recommend checking out the :"
"ref:`introduction_from_other_software` category, which contains guides that "
"will help you get familiar with Krita by comparing its functions to other "
"software."
msgstr ""

#: ../../user_manual/getting_started.rst:11
msgid ""
"If you are new to digital art, just start with :ref:`installation`, which "
"deals with installing Krita, and continue on to :ref:`starting_with_krita`, "
"which helps with making a new document and saving it, :ref:`basic_concepts`, "
"in which we'll try to quickly cover the big categories of Krita's "
"functionality, and finally, :ref:`navigation`, which helps you find basic "
"usage help, such as panning, zooming and rotating."
msgstr ""

#: ../../user_manual/getting_started.rst:13
msgid ""
"When you have mastered those, you can look into the dedicated introduction "
"pages for functionality in the :ref:`user_manual`, read through the "
"overarching concepts behind (digital) painting in the :ref:"
"`general_concepts` section, or just search the :ref:`reference_manual` for "
"what a specific button does."
msgstr ""

#: ../../user_manual/getting_started.rst:15
msgid "Contents:"
msgstr "Obsah:"
