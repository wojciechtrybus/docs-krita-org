# translation of docs_krita_org_contributors_manual.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_contributors_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-01 12:54+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../contributors_manual.rst:5
msgid "Contributors Manual"
msgstr "Príručka prispievateľov"

#: ../../contributors_manual.rst:7
msgid "Everything you need to know to help out with Krita!"
msgstr "Všetko, čo potrebujete vedieť na pomoc s Krita!"

#: ../../contributors_manual.rst:9
msgid "Contents:"
msgstr "Obsah:"
